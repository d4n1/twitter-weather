# Twitter-weather

Twitter-weather check the current weather via Twitter and OpenWeatherMap per week.

## Requirements

- Docker >= 24.0.2
- Docker-Compose >= 2.18.1

## Build

```
docker compose build
```

## Run

```
docker compose up -d
```

or 

```
run.sh
```

Access **localhost:5000/api/twitters**
