class Config(object):
    DEBUG = False
    TESTING = False
    SECRET_KEY = 'twitter-weather'
    MONGO_URI = 'mongodb://db:27017/twitter-weather'


class ProductionConfig(Config):
    MONGO_URI = 'mongodb://db:27017/twitter-weather-production'


class DevelopmentConfig(Config):
    DEBUG = True


class TestingConfig(Config):
    TESTING = True
    MONGO_URI = 'mongodb://db:27017/twitter-weather-test'
