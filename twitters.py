from flask import Blueprint, jsonify, request


bp = Blueprint('twitters', __name__, url_prefix='/api/twitters')

@bp.route('/', methods=['GET'])
def get_all():
    """Get method that get all datas."""
    
    if request.method == 'GET':
        return jsonify(), 200

@bp.route('/<id>', methods=['GET'])
def get_one(id):
    """Get method that get only one data."""

    if request.method == 'GET':
        return jsonify(), 200

@bp.route('/', methods=['POST'])
def post():
    """Post method that save data."""

    if request.method == 'POST':
        return jsonify(), 200

@bp.route('/<id>', methods=['PUT'])
def put(id):
    """Put method that update data."""

    if request.method == 'PUT':
        return jsonify(), 200

@bp.route('/<id>', methods=['DELETE'])
def delete(id):
    """Delete method that delete data."""    

    if request.method == 'DELETE':
        return jsonify(), 200

