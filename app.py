from flask import Flask
from flask_pymongo import PyMongo
from flask_cors import CORS
from twitters import bp as twitters_bp


app = Flask(__name__)

app.config.from_object('config.DevelopmentConfig')

mongo = PyMongo(app)
cors = CORS(app)

app.register_blueprint(twitters_bp)

@app.route("/")
def status():
    return "<h1>On :)</h1>", 200

@app.errorhandler(404)
def page_not_found(e):
    return "<h1>404</h1>", 404

